#!/usr/bin/env bash

./parseback.py -i testfiles/1onc_native.cliq -g testfiles -c testfiles/testing/ &&
    diff -q -r testfiles/testing/ testfiles/1onc_cliqs/ --exclude="*.txt" &&
    echo Finished testing for 1onc_native.cliq file. Diffs were chickens^ &&
    rm testfiles/testing/*
    
./parseback.py -i testfiles/2mta.cliq -g testfiles -c testfiles/testing &&
    diff -q -r testfiles/testing/ testfiles/2mta_cliqs/ --exclude="*.txt" &&
    echo Finished testing for 2mta.cliq file. Diffs were chickens^ &&
    rm testfiles/testing/*

./parseback.py -i testfiles/hybrid_2mta_1onc.cliq -g testfiles -c testfiles/testing/ &&
    diff -r testfiles/testing/ testfiles/hybrid_2mta_1onc_cliqs/ --exclude="*.txt" &&
    echo Finished testing for hybrid_2mta_1onc.cliq file. Diffs were chickens^ &&
    rm testfiles/testing/*

echo All tests over. Check success yourself! :P
